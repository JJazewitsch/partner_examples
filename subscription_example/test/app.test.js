const chai = require("chai");
const expect = chai.expect;
const sinon = require("sinon");
const app = require("../app");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

const KIKSSample = require("../app");
const Repository = require("../src/repository");
const AppDAO = require("../src/dao");
const FHIR = require("../src/fhir");

const request = chai.request(KIKSSample.start);

describe("Application testing", function () {
  var fhirServer, db, repo;

  beforeEach(function () {
    fhirServer = new FHIR("test");
    db = new AppDAO();
  });

  it("First GET should be blank", () => {

    chai
      .request("http://localhost:8081")
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal({});
      });
  });

  it("Should return data after first PUT", async () => {

    chai
      .request("http://localhost:8081")
      .put("/Encounter/123")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal({});
      }); 
    
  });

  it("getData shoule be called once", () => {
    repo = sinon.mock(new Repository(db, fhirServer));
    repo.expects("getData").once();

    chai
      .request("http://localhost:8081")
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal({});
      });
  });
  
  it("setData shoule be called once", () => {
    repo = sinon.mock(new Repository(db, fhirServer));
    repo.expects("setData").once().withArgs("123");

    chai
      .request("http://localhost:8081")
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal({});
      });
  });

});
