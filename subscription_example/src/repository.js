const AppDao = require("./dao");
const FHIR = require("./fhir");

class Repository {
  constructor(database, fhir) {
    this.database = database;
    this.fhir = fhir;
  }

  getData() {
    return new Promise((resolve, reject) => {
      const queries = [];
      let sql = `SELECT * FROM pats`;

      this.database.db.each(
        sql,
        (err, row) => {
          if (err) {
            reject(err);
          } else {
            queries.push(row);
          }
        },
        (err, n) => {
          if (err) {
            reject(err);
          } else {
            resolve(queries);
          }
        }
      );
    });
  }

  async setData(encId) {
    let narrative = await this.fhir.buildNarrative(encId);

    Promise.all([narrative]).then((result) => {
      this.database.saveNarrative(narrative)
    });
    
  }
}

module.exports = Repository;
