const { resolve } = require("bluebird");
const Client = require("fhir-kit-client");

class FHIR {
  constructor(url) {
    this.fhirClient = new Client({
      baseUrl: url,
    });

    this.narrative = "";
  }

  getEncounter(encId) {
    let encounter = this.fhirClient
      .read({
        resourceType: "Encounter",
        id: encId,
      })
      .then((result) => {
        return result;
      });

    return encounter;
  }

  getPatient(patientId) {
    let patient = this.fhirClient
      .read({
        resourceType: "Patient",
        id: patientId,
      })
      .then((result) => {
        return result;
      })
      .catch((error) => {
        console.log("Err" + error);
      });

    return patient;
  }

  getProcedure(patientId) {
    let procedure = this.fhirClient
      .search({
        resourceType: "Procedure",
        searchParams: {
          subject: "Patient/" + patientId,
        },
      })
      .then((result) => {
        return result;
      })
      .catch((error) => {
        console.log("Err" + error);
      });

    return procedure;
  }

  getCondition(patientId) {
    let condition = this.fhirClient
      .search({
        resourceType: "Condition",
        searchParams: {
          subject: "Patient/" + patientId,
        },
      })
      .then((result) => {
        return result;
      })
      .catch((error) => {
        console.log("Err" + error);
      });

    return condition;
  }

  getReferrer(patientId) {
    let referrer = this.fhirClient
      .read({
        resourceType: "Practitioner",
        id: patientId,
      })
      .then((result) => {
        return result;
      })
      .catch((error) => {
        console.log(error);
      });

    return referrer;
  }

  async buildNarrative(encId) {
    
    let encounter = await this.getEncounter(encId);

    let patID = encounter.subject.reference.split("/")[1];
    let practitionerID = encounter.participant[0].individual.reference.split(
      "/"
    )[1];

    let patient = await this.getPatient(patID);
    let procedure = await this.getProcedure(patID);
    let condition = await this.getCondition(patID);
    let referrer = await this.getReferrer(practitionerID);

    let narrative = await Promise.all([
      encounter,
      patient,
      procedure,
      condition,
      referrer,
    ]).then((result) => {
      this.narrative = `Patient ${patient.name[0].family}, ${patient.gender} was admitted on ${encounter.period.start} for an ${encounter.class.display} visit for ${condition.code.text}.The patient was refered from ${referrer.name[0].prefix[0]} ${referrer.name[0].given[0]} ${referrer.name[0].family}. ${procedure.code.text} was performed on him on ${procedure.performedDateTime}\n\n\n`;
    });

    return this.narrative;
  }
}

module.exports = FHIR;
