const express = require("express");
const app = express();

let port = 8081;

class KIKSSample{
  constructor( repository ){
    this.repository = repository;
  }

  start () {
    app.put("/Encounter/:id", (req, res) => {
      //res.sendStatus(200);
      this.repository.setData(req.params.id);
    });
    
    app.get("/", (req, res) => {
        this.repository.getData()
        .then((result) => {
          result.forEach(
            el => res.write(el.data)
          );
          res.end()
        }).finally((result) => {
          res.end()
        });
    })
    
    app.listen(port, () => console.log(`Started server at http://localhost:8081`));
  }
  
  stop() {
    app.close();
  }

}
 
module.exports = KIKSSample